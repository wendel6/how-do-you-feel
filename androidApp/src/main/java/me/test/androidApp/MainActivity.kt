package me.test.androidApp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import me.test.androidApp.databinding.LoginPageBinding


class MainActivity : AppCompatActivity() {
    var fbAuth = FirebaseAuth.getInstance()
    private lateinit var binding: LoginPageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.intro_page)
    }

    fun loginPage(v: View) {
        setContentView(R.layout.login_page)
    }


    fun login(v: View){
        val emailTextContents = findViewById<TextView>(R.id.emailAddress).text.toString()
        val passTextContents = findViewById<TextView>(R.id.pass).text.toString()

        if(emailTextContents == "" || passTextContents == ""){
            showMessage(v,"Error: Field is empty!")
        } else {
            signIn(v, emailTextContents, passTextContents)
        }

    }


    fun signIn(view: View,email: String, password: String){
        showMessage(view,"Authenticating...")

        fbAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, OnCompleteListener<AuthResult> { task ->

            if(task.isSuccessful){
                showMessage(view,"It Worked!!")

                var intent = Intent(this, LoggedInActivity::class.java)
                intent.putExtra("id", fbAuth.currentUser?.email)
                startActivity(intent)

            }else{
                showMessage(view,"Error: ${task.exception?.message}")
            }
        })

    }

    fun showMessage(view:View, message: String){
        Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE).setAction("Action", null).show()
    }
}

